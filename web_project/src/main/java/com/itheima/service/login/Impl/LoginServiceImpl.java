package com.itheima.service.login.Impl;

import com.itheima.dao.system.UserDao;
import com.itheima.domain.system.User;
import com.itheima.factory.MapperFactory;
import com.itheima.service.login.LoginService;
import com.itheima.utils.JedisUtils;
import com.itheima.utils.MD5Util;
import com.itheima.utils.TransactionUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.Random;

public class LoginServiceImpl implements LoginService {
    //根据邮箱和密码查找可用用户
    @Override
    public User login(String email, String password) {
        SqlSession sqlSession = MapperFactory.getSqlSession();
        UserDao mapper = MapperFactory.getMapper(sqlSession, UserDao.class);

        User user = mapper.findByEmailAndPwd(email, password);
        TransactionUtil.close(sqlSession);
        return user;
    }

    //更新密码
    @Override
    public int updatePwd(String email, String password) {
        SqlSession sqlSession = MapperFactory.getSqlSession();
        UserDao mapper = MapperFactory.getMapper(sqlSession, UserDao.class);
        int i = 0;
        try {
            //给密码加密
            password = MD5Util.md5(password);
            i = mapper.updatePwd(email, password);
            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            //e.printStackTrace();
            TransactionUtil.rollback(sqlSession);
        } finally {
            TransactionUtil.close(sqlSession);
        }
        //将影响行数返回给servlet
        return i;
    }

    //获取验证码
    @Override
    public String getCheckCode() {
        Jedis jedis = JedisUtils.getJedis();
        //从redis数据库中获取验证码
        String checkCode = jedis.get("checkCode");
        jedis.close();
        return checkCode;
    }

    //在数据库中设置验证码
    @Override
    public String setCheckCode() {
        Jedis jedis = JedisUtils.getJedis();
        //获取随机的验证码
        String checkCode = CheckCode();
        //在redis数据库中设置验证码
        jedis.set("checkCode", checkCode);
        //设置20秒超时
        jedis.expire("checkCode",10);
        jedis.close();
        return checkCode;
    }

    //获取随机数
    private String CheckCode() {
        String base = "0123456789ABCDEFGabcdefg";
        int size = base.length();
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= 4; i++) {
            //产生0到size-1的随机值
            int index = r.nextInt(size);
            //在base字符串中获取下标为index的字符
            char c = base.charAt(index);
            //将c放入到StringBuffer中去
            sb.append(c);
        }
        return sb.toString();
    }
}
