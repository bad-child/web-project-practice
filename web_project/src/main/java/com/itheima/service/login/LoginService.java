package com.itheima.service.login;

import com.itheima.domain.system.User;

public interface LoginService {
    User login(String eamil, String password);

    int updatePwd(String email, String password);

    String getCheckCode();

    String setCheckCode();
}
