package com.itheima.service.system.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.system.RoleDao;
import com.itheima.domain.system.Role;
import com.itheima.factory.MapperFactory;
import com.itheima.service.system.RoleService;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.UUID;

public class RoleServiceImpl implements RoleService {
    @Override
    public List<Role> findByRoles(String id) {
        SqlSession sqlSession = MapperFactory.getSqlSession();
        RoleDao mapper = sqlSession.getMapper(RoleDao.class);
        List<Role> role = mapper.findRole(id);

        sqlSession.close();
        return role;
    }

    @Override
    public PageInfo<Role> findAll(int page, int size) {
        SqlSession sqlSession = MapperFactory.getSqlSession();
        RoleDao mapper = sqlSession.getMapper(RoleDao.class);
        PageHelper.startPage(page,size);
        List<Role> all = mapper.findAll();

        PageInfo<Role> roleInfo = new PageInfo<>(all);
        sqlSession.close();
        return roleInfo;
    }

    @Override
    public List<Role> findAll() {
        SqlSession sqlSession = MapperFactory.getSqlSession();

        RoleDao mapper = sqlSession.getMapper(RoleDao.class);

        List<Role> all = mapper.findAll();

        sqlSession.commit();
        sqlSession.close();
        return all;
    }

    @Override
    public void save(Role role) {
        SqlSession sqlSession = MapperFactory.getSqlSession();

        RoleDao mapper = sqlSession.getMapper(RoleDao.class);
        String id = UUID.randomUUID().toString();
        role.setId(id);
        mapper.save(role);

        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public void delete(String id) {
        SqlSession sqlSession = MapperFactory.getSqlSession();

        RoleDao mapper = sqlSession.getMapper(RoleDao.class);

        Role role = mapper.findById(id);

        role.setStatus("0");

        mapper.delete(role);

        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public void update(Role role) {
        SqlSession sqlSession = MapperFactory.getSqlSession();

        RoleDao mapper = sqlSession.getMapper(RoleDao.class);

        mapper.update(role);

        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public Role findById(String id) {
        SqlSession sqlSession = MapperFactory.getSqlSession();

        RoleDao mapper = sqlSession.getMapper(RoleDao.class);

        Role role = mapper.findById(id);

        sqlSession.commit();
        sqlSession.close();
        return role;

    }



    @Override
    public Role findByRole(String id) {
        SqlSession sqlSession = MapperFactory.getSqlSession();

        RoleDao mapper = sqlSession.getMapper(RoleDao.class);

        Role role = mapper.findById(id);

        sqlSession.commit();
        sqlSession.close();
        return role;
    }

    @Override
    public void updateRolePermission(String roleId, String permissionIds) {
        SqlSession sqlSession = MapperFactory.getSqlSession();

        RoleDao mapper = sqlSession.getMapper(RoleDao.class);

       mapper.deleteRolePermission(roleId);

        String[] PermissionIdes = permissionIds.split(",");

        for (String permissionId : PermissionIdes) {
            mapper.saveRolePermission(roleId,permissionId);
        }
        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public PageInfo<Role> findSearchName(String searchName, int page, int size) {
        SqlSession sqlSession = MapperFactory.getSqlSession();
        RoleDao mapper = sqlSession.getMapper(RoleDao.class);

         PageHelper.startPage(page,size);
        List<Role> all = mapper.findSearchName(searchName);

        PageInfo<Role> roleInfo = new PageInfo<>(all);
        sqlSession.close();
        return roleInfo;
    }

    @Override
    public void updateStatus(String id) {
        SqlSession sqlSession = MapperFactory.getSqlSession();
        RoleDao mapper = sqlSession.getMapper(RoleDao.class);
        Role role = mapper.findById(id);

        if("0".equals(role.getStatus())){
            role.setStatus("1");
        }else{
            role.setStatus("0");
        }
        mapper.delete(role);
        sqlSession.commit();
        sqlSession.close();

    }
}
