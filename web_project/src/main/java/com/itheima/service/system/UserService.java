package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;

import java.util.List;

public interface UserService {

    void save(User user);

    void delete(User user);

    void deleteById(String id);

    User findById(String id);

    List<Role> findDetail(String id);

    void update(User user);

    List<User> findAll();

    PageInfo findAll(int page, int size);

    PageInfo findByName(String name, int page, int size);

    void addRoleToUser(String id, String[] ids);
}
