package com.itheima.service.system.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.system.SyslogDao;
import com.itheima.domain.system.Syslog;
import com.itheima.factory.MapperFactory;
import com.itheima.service.system.SyslogService;
import com.itheima.utils.TransactionUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class SyslogServiceImpl implements SyslogService {
    @Override
    public void deleteIds(String ids) {
        SqlSession sqlSession = null;
        try{
            //1 获取Sqlsession对象
            sqlSession = MapperFactory.getSqlSession();
            //2 获取dao
            SyslogDao syslogDao = MapperFactory.getMapper(sqlSession, SyslogDao.class);
            // 3 调用dao层操作
            String[] split = ids.split(",");
            for (int i = 0; i < split.length; i++) {
                syslogDao.delete(split[i]);
            }
            //4 提交事务
            TransactionUtil.commit(sqlSession);
        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
            //记录日志
        }finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void save(Syslog syslog) {
        SqlSession sqlSession = null;
        try{
            //1 获取Sqlsession对象
            sqlSession = MapperFactory.getSqlSession();
            //2 获取dao
            SyslogDao syslogDao = MapperFactory.getMapper(sqlSession, SyslogDao.class);
            //id 使用UUID 的生成策略来获取
            String id = UUID.randomUUID().toString();
            syslog.setId(id);
            // 3 调用dao层操作
            syslogDao.save(syslog);
            //4 提交事务
            TransactionUtil.commit(sqlSession);
        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
            //记录日志
        }finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Syslog> findAll() {
        SqlSession sqlSession = null;
        try{
            //1 获取Sqlsession对象
            sqlSession = MapperFactory.getSqlSession();
            //2 获取dao
            SyslogDao syslogDao = MapperFactory.getMapper(sqlSession, SyslogDao.class);
            // 3 调用dao层操作
            return syslogDao.findAll();
        }catch (Exception e){
            throw new RuntimeException(e);
            //记录日志
        }finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public PageInfo findAll(int page, int size) {
        SqlSession sqlSession = null;
        try{
            //1 获取Sqlsession对象
            sqlSession = MapperFactory.getSqlSession();
            //2 获取dao
            SyslogDao syslogDao = MapperFactory.getMapper(sqlSession, SyslogDao.class);
            // 3 调用dao层操作
            PageHelper.startPage(page,size);

            List<Syslog> all =  syslogDao.findAll();
            /*for (Syslog syslog : all) {
                System.out.println(syslog);
            }*/
            PageInfo pageInfo = new PageInfo(all);

            return  pageInfo;
        }catch (Exception e){
            throw new RuntimeException(e);
            //记录日志
        }finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public ByteArrayOutputStream getReport() throws IOException {
        //获取对应要展示的数据
        SqlSession sqlSession = null;
        List<Syslog> sysLogList = null;
        try{
            //1.获取SqlSession
            sqlSession = MapperFactory.getSqlSession();
            //2.获取Dao
            SyslogDao sysLogMapper = MapperFactory.getMapper(sqlSession,SyslogDao.class);
            //3.调用Dao层操作
            sysLogList = sysLogMapper.findAll();
        }catch (Exception e){
            throw new RuntimeException(e);
            //记录日志
        }finally {
            try {
                TransactionUtil.close(sqlSession);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        //1.获取到对应的Excel文件，工作簿文件
        Workbook wb = new XSSFWorkbook();
        //2.创建工作表
        Sheet s = wb.createSheet("日志文件");

        //设置通用配置
//        s.setColumnWidth(4,100);
        CellStyle cs_field = wb.createCellStyle();
        cs_field.setAlignment(HorizontalAlignment.CENTER);
        cs_field.setBorderTop(BorderStyle.THIN);
        cs_field.setBorderBottom(BorderStyle.THIN);
        cs_field.setBorderLeft(BorderStyle.THIN);
        cs_field.setBorderRight(BorderStyle.THIN);

        //制作标题
        s.addMergedRegion(new CellRangeAddress(1,1,1,7));

        Row row_1 = s.createRow(1);
        Cell cell_1_1 = row_1.createCell(1);
        cell_1_1.setCellValue("在线试题导出信息");
        //创建一个样式
        CellStyle cs_title = wb.createCellStyle();
        cs_title.setAlignment(HorizontalAlignment.CENTER);
        cs_title.setVerticalAlignment(VerticalAlignment.CENTER);
        cell_1_1.setCellStyle(cs_title);

        //制作表头
        String[] fields = {"ID","VisitTime","username","ip","url",
                "executionTime","method"};
        Row row_2 = s.createRow(2);

        for (int i = 0; i < fields.length; i++) {
            Cell cell_2_temp = row_2.createCell(1 + i); //++
            cell_2_temp.setCellValue(fields[i]);    //++
            cell_2_temp.setCellStyle(cs_field);
        }

        //制作数据区
        int row_index = 0;
        for (Syslog q : sysLogList) {
            int cell_index = 0;
            Row row_temp = s.createRow(3 + row_index++);

            Cell cell_data_1 = row_temp.createCell(1 + cell_index++);
            cell_data_1.setCellValue(q.getId());    //++
            cell_data_1.setCellStyle(cs_field);

            Cell cell_data_2 = row_temp.createCell(1 + cell_index++);
            cell_data_2.setCellValue(q.getVisitTime());    //++
            cell_data_2.setCellStyle(cs_field);

            Cell cell_data_3 = row_temp.createCell(1 + cell_index++);
            cell_data_3.setCellValue(q.getUsername());    //++
            cell_data_3.setCellStyle(cs_field);

            Cell cell_data_4 = row_temp.createCell(1 + cell_index++);
            cell_data_4.setCellValue(q.getIp());    //++
            cell_data_4.setCellStyle(cs_field);

            Cell cell_data_5 = row_temp.createCell(1 + cell_index++);
            cell_data_5.setCellValue(q.getUrl());    //++
            cell_data_5.setCellStyle(cs_field);

            Cell cell_data_7 = row_temp.createCell(1 + cell_index++);
            cell_data_7.setCellValue(q.getExecutionTime());    //++
            cell_data_7.setCellStyle(cs_field);

            Cell cell_data_8 = row_temp.createCell(1 + cell_index++);
            cell_data_8.setCellValue(q.getMethod());    //++
            cell_data_8.setCellStyle(cs_field);

        }

        //将内存中的workbook数据写入到流中
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        wb.write(os);
        wb.close();
        return os;
    }

    @Override
    public PageInfo<Syslog> findByTime(String values,int page, int size ) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            SyslogDao mapper = MapperFactory.getMapper(sqlSession, SyslogDao.class);
            PageHelper.startPage(page, size);
            List<Syslog> all = mapper.findByTime("%"+values+"%");
            return new PageInfo<>(all);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }
}
