package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Permission;

import java.util.List;
import java.util.Map;

public interface PermissionService {
    void deleteById(String id);

    void deleteSelected(String[] ids);

    void save(Permission permission);

    void update(Permission permission);

    List<Permission> findAll();

    Permission findById(String id);

    List<Map> findAuthorDataByRoleId(String roleId);

    PageInfo findAll(int page, int size);

    void closeSelected(String ids);

    void openSelected(String ids);

    List<Permission> findPermissionByUserId(String userId);

    PageInfo<Permission> findByName(String searchName, int page, int size);
}
