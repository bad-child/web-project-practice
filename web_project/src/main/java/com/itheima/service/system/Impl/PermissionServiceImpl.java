package com.itheima.service.system.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.system.PermissionDao;
import com.itheima.dao.system.UserDao;
import com.itheima.domain.data.Traveller;
import com.itheima.domain.system.Permission;
import com.itheima.domain.system.User;
import com.itheima.factory.MapperFactory;
import com.itheima.service.system.PermissionService;
import com.itheima.utils.MD5Util;
import com.itheima.utils.TransactionUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PermissionServiceImpl implements PermissionService {
    @Override
    public void deleteById(String id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);

            permissionDao.deleteById(id);

            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }

    @Override
    public void deleteSelected(String[] ids) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);

            for (String id : ids) {
                permissionDao.deleteById(id);
            }

            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }

    @Override
    public void save(Permission permission) {
        SqlSession sqlSession = null;

        try {
            sqlSession = MapperFactory.getSqlSession();
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);

            String id = UUID.randomUUID().toString();
            permission.setPermissionId(id);

            permissionDao.save(permission);

            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }

    @Override
    public void update(Permission permission) {
        SqlSession sqlSession = null;
        try {
            //1.获取SqlSession
            sqlSession = MapperFactory.getSqlSession();
            //2.获取Dao
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);
            //3.调用Dao层操作
            permissionDao.update(permission);
            //4.提交事务
            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
            //记录日志
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Permission> findAll() {
        SqlSession sqlSession = null;
        try {
            //1.获取SqlSession
            sqlSession = MapperFactory.getSqlSession();
            //2.获取Dao
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);
            //3.调用Dao层操作
            return permissionDao.findAll();
        } catch (Exception e) {
            throw new RuntimeException(e);
            //记录日志
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Permission findById(String id) {
        SqlSession sqlSession = null;
        try {
            //1.获取SqlSession
            sqlSession = MapperFactory.getSqlSession();
            //2.获取Dao
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);
            //3.调用Dao层操作
            return permissionDao.findById(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
            //记录日志
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public List<Map> findAuthorDataByRoleId(String roleId) {

        SqlSession sqlSession = null;
        try {
            //1.获取SqlSession
            sqlSession = MapperFactory.getSqlSession();
            //2.获取Dao
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);
            //3.调用Dao层操作
            return permissionDao.findAuthorDataByRoleId(roleId);
        } catch (Exception e) {
            throw new RuntimeException(e);
            //记录日志
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public PageInfo findAll(int page, int size) {
        SqlSession sqlSession = null;
        try {
            //1.获取SqlSession
            sqlSession = MapperFactory.getSqlSession();
            //2.获取Dao
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);
            //3.调用Dao层操作
            PageHelper.startPage(page, size);
            List<Permission> all = permissionDao.findAll();
            PageInfo pageInfo = new PageInfo(all);
            return pageInfo;
        } catch (Exception e) {
            throw new RuntimeException(e);
            //记录日志
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void closeSelected(String ids) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);

            String[] arr = ids.split(",");

            for (String id : arr) {
                permissionDao.closeState(id);
            }

            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }

    }

    @Override
    public void openSelected(String ids) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);

            String[] arr = ids.split(",");

            for (String id : arr) {
                permissionDao.openState(id);
            }

            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }

    @Override
    public List<Permission> findPermissionByUserId(String userId) {
        SqlSession sqlSession = MapperFactory.getSqlSession();
        PermissionDao mapper = MapperFactory.getMapper(sqlSession, PermissionDao.class);
        return    mapper.findPermissionByUserId(userId);
    }

    @Override
    public PageInfo<Permission> findByName(String searchName, int page, int size) {
        SqlSession sqlSession = null;
        try {
            //1.获取SqlSession
            sqlSession = MapperFactory.getSqlSession();
            //2.获取Dao
            PermissionDao permissionDao = MapperFactory.getMapper(sqlSession, PermissionDao.class);
            //3.调用Dao层操作
            PageHelper.startPage(page, size);
            List<Permission> all = permissionDao.findByName(searchName);
            PageInfo pageInfo = new PageInfo(all);
            return pageInfo;
        } catch (Exception e) {
            throw new RuntimeException(e);
            //记录日志
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
