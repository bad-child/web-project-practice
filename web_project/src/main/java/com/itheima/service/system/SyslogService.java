package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Syslog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public interface SyslogService {
    void deleteIds(String ids);

    void save(Syslog syslog);

    List<Syslog> findAll();

    PageInfo findAll(int page, int size);

    ByteArrayOutputStream getReport() throws IOException;

    PageInfo<Syslog> findByTime(String values,int page, int size);
}
