package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;

import java.util.List;

public interface RoleService {
    PageInfo<Role> findAll(int page, int size);

    List<Role> findAll();

    void save(Role role);

    void delete(String id);

    void update(Role role);

    Role findById(String id);

    Role findByRole(String id);

    List<Role> findByRoles(String id);

    void updateRolePermission(String roleId, String permissionIds);

    PageInfo<Role> findSearchName(String searchName, int page, int size);

    void updateStatus(String id);
}
