package com.itheima.service.data;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.data.Orders;

import java.util.List;

public interface OrdersService {
    /**
     * 普通查询所有
     * @return
     */
    List<Orders> findAll();

    /**
     * 分页查询
     * @param page 当前页
     * @param size  每页显示多少条
     * @return
     */
    PageInfo<Orders> findAll(int page, int size);

    /**
     * 通过id查找
     * @param orders orders对象
     * @return
     */
    Orders findById(Orders orders);

    void save(Orders orders);

    /**
     * 伪删除
     * @param ids   id数组
     */
    void delete(String ids);

    void update(Orders orders);


    /**
     * 开启
     * @param ids   id数组
     */
    void open(String ids);

    /**
     * 屏蔽
     * @param ids   id数组
     */
    void close(String ids);

    PageInfo<Orders> findByName(String searchName, int page, int size);
}
