package com.itheima.service.data;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.data.Product;

import java.util.List;

public interface ProductService {
    void save(Product product);

    void update(Product product);

    void delete(String id);

    Product findById(String id);

    List<Product> findAll();

    PageInfo findAll(int page, int size);

    void open(String ids);

    void close(String ids);

    PageInfo <Product> findByNum(String productNum, int page, int size);
}
