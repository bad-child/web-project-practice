package com.itheima.service.data;

import com.itheima.domain.data.Member;

import java.util.List;

public interface MemberService {
    List<Member> findAll();
}
