package com.itheima.service.data.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.data.MemberDao;
import com.itheima.dao.data.OrdersDao;
import com.itheima.domain.data.Member;
import com.itheima.domain.data.Orders;
import com.itheima.factory.MapperFactory;
import com.itheima.service.data.MemberService;
import com.itheima.utils.TransactionUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class MemberServiceImpl implements MemberService {
    @Override
    public List<Member> findAll() {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            MemberDao mapper = MapperFactory.getMapper(sqlSession, MemberDao.class);

            List<Member> all = mapper.findAll();

            return all;
        } catch (Exception e) {

            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }
}
