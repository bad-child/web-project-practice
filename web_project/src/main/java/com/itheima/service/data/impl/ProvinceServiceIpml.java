package com.itheima.service.data.impl;



import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.data.ProductDao;
import com.itheima.dao.data.ProvinceDao;
import com.itheima.domain.data.Product;
import com.itheima.domain.data.Province;
import com.itheima.factory.MapperFactory;
import com.itheima.service.data.ProvinceService;
import com.itheima.utils.TransactionUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class ProvinceServiceIpml implements ProvinceService {
    @Override
    public List<Province> findAll(){
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            ProvinceDao mapper = MapperFactory.getMapper(sqlSession, ProvinceDao.class);
            List<Province> all = mapper.findAll();
            return all;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
