package com.itheima.service.data.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.data.OrdersDao;
import com.itheima.domain.data.Orders;
import com.itheima.domain.data.Traveller;
import com.itheima.factory.MapperFactory;
import com.itheima.service.data.OrdersService;
import com.itheima.utils.TransactionUtil;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.ibatis.session.SqlSession;

import java.text.SimpleDateFormat;
import java.util.*;

public class OrdersServiceImpl implements OrdersService {
    @Override
    public List<Orders> findAll() {
        return null;
    }

    @Override
    public PageInfo<Orders> findAll(int page, int size) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            OrdersDao mapper = MapperFactory.getMapper(sqlSession, OrdersDao.class);
            PageHelper.startPage(page, size);

            List<Orders> all = mapper.findAll();

            PageInfo<Orders> pageInfo = new PageInfo<>(all);
            return pageInfo;
        } catch (Exception e) {

            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }

    @Override
    public Orders findById(Orders orders) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            OrdersDao mapper = MapperFactory.getMapper(sqlSession, OrdersDao.class);

            Orders byId = mapper.findById(orders);

            if (byId != null) {
                for (Traveller traveller : byId.getTravellers()) {
                    traveller.setCredentialsTypeStr(traveller.getCredentialsType().toString());
                }

            }
            return byId;
        } catch (Exception e) {

            throw new RuntimeException(e);
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void save(Orders orders) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            OrdersDao mapper = MapperFactory.getMapper(sqlSession, OrdersDao.class);
            orders.setId(UUID.randomUUID().toString());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            orders.setOrderTime(sdf.format(new Date()));
            orders.setOrderNum(getNum());
            orders.setOrderStatus(1);
            orders.setOs("1");

            mapper.save(orders);
            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void delete(String ids) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            OrdersDao mapper = MapperFactory.getMapper(sqlSession, OrdersDao.class);
            String[] split = ids.split(",");

            mapper.changeOsByIds(split);
            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(Orders orders) {
    }

    @Override
    public void open(String ids) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            OrdersDao mapper = MapperFactory.getMapper(sqlSession, OrdersDao.class);
            String[] split = ids.split(",");

            mapper.openOrderStatus(split);
            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void close(String ids) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            OrdersDao mapper = MapperFactory.getMapper(sqlSession, OrdersDao.class);
            String[] split = ids.split(",");

            mapper.closeOrderStatus(split);
            TransactionUtil.commit(sqlSession);
        } catch (Exception e) {
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        } finally {
            try {
                TransactionUtil.close(sqlSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public PageInfo<Orders> findByName(String searchName, int page, int size) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MapperFactory.getSqlSession();
            OrdersDao mapper = MapperFactory.getMapper(sqlSession, OrdersDao.class);
            PageHelper.startPage(page, size);

            List<Orders> all = mapper.findByName("%"+searchName+"%");


            PageInfo<Orders> pageInfo = new PageInfo<>(all);
            return pageInfo;
        } catch (Exception e) {

            throw new RuntimeException(e);
        } finally {
            TransactionUtil.close(sqlSession);
        }
    }


    public String getNum(){
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < 5; i++) {
            int m = (int) (Math.random()*10);
            sb.append(m+"");
        }
        return sb.toString();
    }
}
