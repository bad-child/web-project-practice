package com.itheima.service.data;


import com.itheima.domain.data.Province;

import java.util.List;

public interface ProvinceService {
    List<Province> findAll();
}
