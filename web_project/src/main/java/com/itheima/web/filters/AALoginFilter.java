package com.itheima.web.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(value = "/*")
public class AALoginFilter implements Filter {

    private FilterConfig filterConfig;

    /**
     * 初始化方法，获取过滤器的配置对象
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        //1.定义和协议相关的请求和响应对象
        HttpServletRequest request ;
        HttpServletResponse response;
        try{
            //2.把参数转换成协议相关的对象
            request = (HttpServletRequest)req;
            response = (HttpServletResponse)resp;
            String requestURI = request.getRequestURI();
            //System.out.println(requestURI);
            boolean check = true;
            if (requestURI.contains("login")
                    || requestURI.endsWith(".js")
                    || requestURI.endsWith(".ico")
                    || requestURI.endsWith(".png")
                    || requestURI.endsWith(".jpg")
                    || requestURI.endsWith("index.jsp")
                    || requestURI.endsWith("author.jsp")
                    || requestURI.endsWith("unauthorized.jsp")
                    || requestURI.endsWith("failer.jsp")
                    || requestURI.endsWith("updatePwd.jsp")
                    ||requestURI.contains("failer")
                    || requestURI.contains("index")
                    || requestURI.endsWith("jpg")
                    || requestURI.contains("updatePwd")
                    || requestURI.contains("Filter")
                    || requestURI.endsWith("checkCode")
                    ||requestURI.endsWith("/"))

            {
                check = false;
            }

            if (check) {
                //登录验证  判断session域是否有用户名
                Object loginname = request.getSession().getAttribute("loginname");
                if (loginname == null || loginname.toString().equals("")) {
                    response.sendRedirect(request.getContextPath() + "/index.jsp");
                    return;
                }
            }
            chain.doFilter(request,response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        //可以做一些清理操作
    }
}
