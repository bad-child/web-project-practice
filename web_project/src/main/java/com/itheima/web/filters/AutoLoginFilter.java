package com.itheima.web.filters;

import com.itheima.domain.system.Permission;
import com.itheima.domain.system.User;
import com.itheima.service.login.Impl.LoginServiceImpl;
import com.itheima.service.login.LoginService;
import com.itheima.service.system.Impl.PermissionServiceImpl;
import com.itheima.service.system.Impl.UserServiceImpl;
import com.itheima.service.system.PermissionService;
import com.itheima.service.system.UserService;
import com.itheima.utils.MD5Util;
import com.itheima.web.controller.login.LoginServlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebFilter(value = "/*")
public class AutoLoginFilter implements Filter {

    private FilterConfig filterConfig;

    /**
     * 初始化方法，获取过滤器的配置对象
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        //1.定义和协议相关的请求和响应对象
        HttpServletRequest request;
        HttpServletResponse response;
        try {
            //2.把参数转换成协议相关的对象
            request = (HttpServletRequest) req;
            response = (HttpServletResponse) resp;
            //1.获取本次操作
            String url = request.getRequestURI();

            if(url.endsWith(".css")
                    || url.endsWith(".js")
                    || url.endsWith("/")
                    || url.endsWith(".png")
                    || url.endsWith(".jpg")
                    || url.endsWith(".ico")
                    || url.endsWith("index.jsp")
                    || url.endsWith("updatePwd.jsp")
                    || url.endsWith("unauthorized.jsp")
                    || url.contains("Filter")){
                chain.doFilter(request,response);
                return;
            }
            String queryString = request.getQueryString();
            if(queryString != null) {
                if (queryString.endsWith("operation=home")
                        || queryString.endsWith("operation=login")
                        || queryString.endsWith("operation=updatePwd")
                        || queryString.endsWith("operation=setCheckCode")
                        || queryString.endsWith("operation=logout")) {
                    chain.doFilter(request, response);
                    return;
                }
            }

            //获取session里的loginname
            Object loginname = request.getSession().getAttribute("loginname");
            //判断访问的资源是否为登陆页面
            // 若为登陆页面则判断是否已登陆
            // 若已登陆则跳转至main.jsp
            if (url.contains("login")) {
                if (!(loginname == null || loginname.toString().equals(""))) {
                    req.getRequestDispatcher("/WEB-INF/pages/home/main.jsp").forward(req, resp);
                    return;
                }
            }

            //判断是否已登陆
            if (loginname == null || loginname.toString().equals("")) {
                //获取是否设置自动登陆
                Cookie[] cookies = request.getCookies();
                if (cookies != null) {
                    String autoLogin = null;
                    String emailAndPwd = null;
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals("autoLogin")) {
                            autoLogin = cookie.getValue();
                        }
                        if (cookie.getName().equals("email&password")) {
                            emailAndPwd = cookie.getValue();
                        }
                    }
                    //若autoLogin的值为"true",则自动登陆
                    if ("true".equals(autoLogin)) {
                        //获取账号和密码
                        String[] split = emailAndPwd.split("&");
                        String eamil = split[0];
                        String pwd = split[1];
                        //给密码加密
                        pwd = MD5Util.md5(pwd);
                        LoginService loginService = new LoginServiceImpl();
                        User user = loginService.login(eamil,pwd);
                        if (user!=null) {
                            request.getSession().setAttribute("loginname", user.getUsername());
                            if (url.contains("login")) {
                                PermissionService permissionService =  new PermissionServiceImpl();;

                                List<Permission> permissionList = permissionService.findPermissionByUserId(user.getId());
                                request.setAttribute("permissionList",permissionList);
                                //当前登录用户对应的可操作模块的所有url拼接成一个大的字符串
                                StringBuffer sbf = new StringBuffer();
                                for (Permission permission : permissionList) {
                                    sbf.append(permission.getUrl());
                                    sbf.append(',');
                                }
                                request.getSession().setAttribute("authorStr",sbf.toString());
                                req.getRequestDispatcher("/WEB-INF/pages/home/main.jsp").forward(req, resp);
                                return;
                            }
                            PermissionService permissionService =  new PermissionServiceImpl();;

                            List<Permission> permissionList = permissionService.findPermissionByUserId(user.getId());
                            request.setAttribute("permissionList",permissionList);
                            //当前登录用户对应的可操作模块的所有url拼接成一个大的字符串
                            StringBuffer sbf = new StringBuffer();
                            for (Permission permission : permissionList) {
                                sbf.append(permission.getUrl());
                                sbf.append(',');
                            }
                            request.getSession().setAttribute("authorStr",sbf.toString());
                            //自动登陆 放行
                            chain.doFilter(request, response);
                        } else {
                            req.getRequestDispatcher("/user/login?operation=logout").forward(req,resp);
                            //登陆失败 清除Cookie储存的信息 放行
                            chain.doFilter(request, response);
                        }
                    } else {
                        //未设置自动登陆 放行
                        chain.doFilter(request, response);
                    }
                } else {
                    //首次访问 放行
                    chain.doFilter(request, response);
                }
            } else {
                //已登录 放行
                chain.doFilter(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        //可以做一些清理操作
    }
}
