package com.itheima.web.controller.data;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.data.Orders;
import com.itheima.domain.data.Product;
import com.itheima.domain.data.Province;
import com.itheima.service.data.impl.ProvinceServiceIpml;
import com.itheima.service.data.ProvinceService;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/data/product")
public class ProductServlet extends BaseServlet {

    protected void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;

        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }


        if (StringUtils.isNotBlank(request.getParameter("searchName"))) {
            String searchName = request.getParameter("searchName");
            PageInfo<Product> all = productService.findByNum(searchName, page, size);
            all.setSize(size);
            request.setAttribute("page", all);
            request.setAttribute("searchName", searchName);

        } else {
            PageInfo<Product> all = productService.findAll(page, size);
            all.setSize(size);
            request.setAttribute("page", all);
        }
        request.getRequestDispatcher("/WEB-INF/pages/data/product/product-list.jsp").forward(request, response);
    }

    public void toAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProvinceService province = new ProvinceServiceIpml();
        List<Province> provinces = province.findAll();
        request.setAttribute("provinces", provinces);
        request.getRequestDispatcher("/WEB-INF/pages/data/product/product-add.jsp").forward(request, response);
    }

    public void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Product product = BeanUtil.fillBean(request, Product.class, "yyyy-MM-dd");
        productService.save(product);
        response.sendRedirect(request.getContextPath() + "/data/product?operation=list");
    }
    public void toEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        Product product = productService.findById(id);
        request.setAttribute("product", product);
        ProvinceService province = new ProvinceServiceIpml();
        List<Province> provinces = province.findAll();
        request.setAttribute("provinces", provinces);
        request.getRequestDispatcher("/WEB-INF/pages/data/product/product-update.jsp").forward(request, response);
    }

    public void edit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ProvinceService province = new ProvinceServiceIpml();
        List<Province> provinces = province.findAll();
        request.setAttribute("provinces", provinces);
        //将数据获取到，封装成一个对象
        Product Product = BeanUtil.fillBean(request, Product.class, "yyyy-MM-dd");
        //调用业务层接口save
        productService.update(Product);
        //跳转回到页面list
        response.sendRedirect(request.getContextPath() + "/data/product?operation=list");
    }

    public void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //将数据获取到，封装成一个对象
        String ids = request.getParameter("ids");
        //调用业务层接口save
        productService.delete(ids);
        response.sendRedirect(request.getContextPath() + "/data/product?operation=list");
    }

    public void open(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ids = request.getParameter("ids");
        //调用业务层接口save
        productService.open(ids);
        response.sendRedirect(request.getContextPath() + "/data/product?operation=list");
    }

    public void close(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ids = request.getParameter("ids");
        //调用业务层接口save
        productService.close(ids);
        response.sendRedirect(request.getContextPath() + "/data/product?operation=list");
    }

    public void findByNum(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;
        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        String productNum = request.getParameter("productNum");
        System.out.println(productNum);
        if (productNum != null && productNum != "") {
            PageInfo<Product> byNum = productService.findByNum(productNum, page, size);
            request.setAttribute("page", byNum);
        }
        request.getRequestDispatcher("/WEB-INF/pages/data/product/product-list.jsp").forward(request, response);
    }

}