package com.itheima.web.controller.data;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.data.Member;
import com.itheima.domain.data.Orders;
import com.itheima.domain.data.Product;
import com.itheima.service.data.MemberService;
import com.itheima.service.data.impl.MemberServiceImpl;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@WebServlet("/data/orders")
public class OrderServlet extends BaseServlet {


    private void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;

        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }


        if (StringUtils.isNotBlank(request.getParameter("searchName"))) {
            String searchName = request.getParameter("searchName");
            PageInfo<Orders> all = ordersService.findByName(searchName, page, size);
            all.setSize(size);
            request.setAttribute("page", all);
            request.setAttribute("searchName", searchName);

        } else {
            PageInfo<Orders> all = ordersService.findAll(page, size);
            all.setSize(size);
            request.setAttribute("page", all);
        }
        request.getRequestDispatcher("/WEB-INF/pages/data/orders/orders-list.jsp").forward(request, response);

    }


    private void toAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MemberService memberService = new MemberServiceImpl();
        List<Member> memberList = memberService.findAll();
        request.setAttribute("memberList",memberList);
        List<Product> all = productService.findAll();
        List<Orders> ordersList = ordersService.findAll();
        request.setAttribute("ordersList",ordersList);
        request.setAttribute("productList", all);
        request.getRequestDispatcher("/WEB-INF/pages/data/orders/orders-add.jsp").forward(request, response);
    }

    private void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Orders orders = BeanUtil.fillBean(request, Orders.class, "yyyy-MM-dd");
        System.out.println(orders);
        ordersService.save(orders);
        response.sendRedirect(request.getContextPath() + "/data/orders?operation=list");
    }


    private void toDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Orders orders = BeanUtil.fillBean(request, Orders.class, "yyyy-MM-dd");

        Orders byId = ordersService.findById(orders);
        request.setAttribute("orders", byId);

        request.getRequestDispatcher("/WEB-INF/pages/data/orders/orders-show.jsp").forward(request, response);

    }

    private void deleteByIds(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ids = request.getParameter("ids");

        ordersService.delete(ids);

        response.sendRedirect(request.getContextPath() + "/data/orders?operation=list");
    }

    private void open(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ids = request.getParameter("ids");
        ordersService.open(ids);
        response.sendRedirect(request.getContextPath() + "/data/orders?operation=list");
    }

    private void close(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ids = request.getParameter("ids");
        ordersService.close(ids);
        response.sendRedirect(request.getContextPath() + "/data/orders?operation=list");
    }


    private void list1(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;

        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }


        PageInfo<Orders> all = ordersService.findAll(page, size);
        all.setSize(size);
        request.setAttribute("page", all);
        request.getRequestDispatcher("/WEB-INF/pages/data/orders/orders-list.jsp").forward(request, response);

    }
}
