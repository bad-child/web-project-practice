package com.itheima.web.controller.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Permission;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

@WebServlet("/system/role")
public class RoleServlet extends BaseServlet {
    //list查询所有
    public void list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int page = 1;
        int size = 5;

        if (StringUtils.isNotBlank(req.getParameter("page"))) {
            page = Integer.parseInt(req.getParameter("page"));
            System.out.println(page+"page");
        }
        if (StringUtils.isNotBlank(req.getParameter("size"))) {
            size = Integer.parseInt(req.getParameter("size"));
            System.out.println(size+"size");
        }
        if(StringUtils.isNotBlank(req.getParameter("searchName"))){
            String searchName = req.getParameter("searchName");

            PageInfo<Role> roleList = roleService.findSearchName( searchName,page, size);

            req.setAttribute("page",roleList);
            req.setAttribute("searchName",searchName);
            roleList.setSize(size);
            req.getRequestDispatcher("/WEB-INF/pages/system/role/role-list.jsp").forward(req,resp);
            return;
        }
        PageInfo<Role> roleList = roleService.findAll(page, size);
        req.setAttribute("page",roleList);
        roleList.setSize(size);
        req.getRequestDispatcher("/WEB-INF/pages/system/role/role-list.jsp").forward(req,resp);
    }

    public void findById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");

        Role role = roleService.findById(id);

        List<User> users = role.getUsers();

        req.setAttribute("users",users);
        req.setAttribute("role", role);


        Role role2 = roleService.findByRole(id);
        req.setAttribute("role",role2);
        List<Map> map = permissionService.findAuthorDataByRoleId(id);
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(map);

        req.setAttribute("rolePermissionJson",json);
        req.getRequestDispatcher("/WEB-INF/pages/system/role/details.jsp").forward(req,resp);


    }


    public void author(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        Role role = roleService.findByRole(id);
        req.setAttribute("role",role);
        List<Map> map = permissionService.findAuthorDataByRoleId(id);
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(map);
        System.out.println(json);
        req.setAttribute("rolePermissionJson",json);
        req.getRequestDispatcher("/WEB-INF/pages/system/role/author.jsp").forward(req,resp);

    }


    public void updateRolePermission(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String roleId = req.getParameter("roleId");
        String permissionIds = req.getParameter("permissionIds");

        roleService.updateRolePermission(roleId,permissionIds);

        list(req,resp);

    }


    //toAdd
    public void toAdd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/system/role/role-add.jsp").forward(req,resp);
    }

    public void save(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Role role = BeanUtil.fillBean(req, Role.class);
        roleService.save(role);
        list(req,resp);

    }

    //编辑回显
    public void toEdit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("111111");
        String id = req.getParameter("id");
        System.out.println(id);
        Role role = roleService.findById(id);
        role.setUsers(null);
        System.out.println(role);
        req.setAttribute("role",role);

        req.getRequestDispatcher("/WEB-INF/pages/system/role/role-update.jsp").forward(req,resp);
    }

    //修改角色信息
    public void edit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, InvocationTargetException, IllegalAccessException {

        Role role = BeanUtil.fillBean(req, Role.class);

        roleService.update(role);
        list(req,resp);

    }

    public void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] ids = req.getParameterValues("ids");
        System.out.println(ids);

        for (String id : ids) {
            roleService.delete(id);
        }
        list(req,resp);
    }

    public void updateStatus(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        System.out.println(id);
        roleService.updateStatus(id);
        list(req,resp);
    }

}
