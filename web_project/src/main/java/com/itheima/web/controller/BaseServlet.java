package com.itheima.web.controller;


import com.itheima.domain.system.Syslog;
import com.itheima.service.data.OrdersService;
import com.itheima.service.data.ProductService;
import com.itheima.service.data.impl.OrdersServiceImpl;
import com.itheima.service.data.impl.ProductServiceImpl;
import com.itheima.service.login.Impl.LoginServiceImpl;
import com.itheima.service.login.LoginService;
import com.itheima.service.system.Impl.PermissionServiceImpl;
import com.itheima.service.system.Impl.RoleServiceImpl;
import com.itheima.service.system.Impl.SyslogServiceImpl;
import com.itheima.service.system.Impl.UserServiceImpl;
import com.itheima.service.system.PermissionService;
import com.itheima.service.system.RoleService;
import com.itheima.service.system.SyslogService;
import com.itheima.service.system.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseServlet extends HttpServlet {
    protected OrdersService ordersService;
    protected LoginService loginService;
    protected PermissionService permissionService;
    protected SyslogService syslogService;
    protected ProductService productService;
    protected RoleService roleService;
    protected UserService userService;

    @Override
    public void init() throws ServletException {
        ordersService = new OrdersServiceImpl();
        loginService = new LoginServiceImpl();
        permissionService = new PermissionServiceImpl();
        syslogService = new SyslogServiceImpl();
        productService = new ProductServiceImpl();
        roleService = new RoleServiceImpl();
        userService = new UserServiceImpl();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //访问时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH/mm/ss");
        String visitTimeStr = sdf.format(date);
        boolean flag = false;
        long start = 0;
        String loginname = null;
        String ip = null;
        String url = null;

        //访问用户
        try {
            loginname = request.getSession().getAttribute("loginname").toString();
            //访问ip
            ip = request.getRemoteAddr();
            //资源url
            url = request.getRequestURL().toString();
            //记录开始时间
            start = System.currentTimeMillis();
            flag = true;
        } catch (Exception e) {
            //e.printStackTrace();
        }

        String operation = null;
        try {
            operation = request.getParameter("operation");
            Class<? extends BaseServlet> clazz = this.getClass();
            Method method = clazz.getDeclaredMethod(operation, HttpServletRequest.class, HttpServletResponse.class);
            method.setAccessible(true);
            method.invoke(this, request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (flag) {
            //记录结束时间
            long end = System.currentTimeMillis();
            //开始时间-结束时间得到运行时间(执行时间)
            long execytionTime = end - start;
            //判断
            if (operation.contains("list")
                    || operation.equals("findById")
                    || operation.equals("list1")
                    || operation.equals("findByNum")
                    || operation.equals("findByName")
                    || operation.equals("toEdit")
                    || operation.equals("toAdd")
                    || operation.equals("home")
                    || operation.equals("logout")
                    || operation.equals("login")
                    || operation.equals("deleteIds")
                    || operation.equals("findByTime")
                    || operation.equals("findAll")) {
            }else{
                Syslog syslog = new Syslog();
                syslog.setUsername(loginname);
                syslog.setVisitTime(date);
                syslog.setVisitTimeStr(visitTimeStr);
                syslog.setIp(ip);
                syslog.setUrl(url);
                syslog.setExecutionTime(execytionTime);
                syslog.setMethod(operation);
                //将数据封装成对象 调用service的save方法保存到数据库
                syslogService.save(syslog);
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        this.doGet(request, response);
    }
}
