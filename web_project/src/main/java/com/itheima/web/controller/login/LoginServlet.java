package com.itheima.web.controller.login;

import com.itheima.domain.system.Permission;
import com.itheima.domain.system.User;
import com.itheima.utils.MD5Util;
import com.itheima.web.controller.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user/login")
public class LoginServlet extends BaseServlet {
    //登陆
    private void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取参数
        String email = req.getParameter("email");
        String pwd = req.getParameter("password");
        String ck = req.getParameter("ck");
        String autologin = req.getParameter("autologin");
        //给密码加密
        String password = MD5Util.md5(pwd);
        //根据邮箱和密码查找用户
        User user = loginService.login(email, password);


        if (!(user == null)) {
            //登陆成功
            //判断是否记住账号密码
            if ("1".equals(ck)) {
                //在cookie中设置ck
                Cookie ckCookie = new Cookie("ck", "1");
                ckCookie.setMaxAge(36000);
                ckCookie.setPath("/");
                resp.addCookie(ckCookie);
                //在cookie中设置账号&密码
                Cookie emailAndPasswordCookie = new Cookie("email&password", email + "&" + pwd);
                emailAndPasswordCookie.setMaxAge(36000);
                emailAndPasswordCookie.setPath("/");
                resp.addCookie(emailAndPasswordCookie);
                Cookie autoLoginCookie = null;
                //判断是否自动登陆
                if ("1".equals(autologin)) {
                    //在cookie中设置自动登陆
                    autoLoginCookie = new Cookie("autoLogin", "true");
                } else {
                    autoLoginCookie = new Cookie("autoLogin", "false");
                }
                autoLoginCookie.setMaxAge(36000);
                autoLoginCookie.setPath("/");
                resp.addCookie(autoLoginCookie);
            }
            req.getSession().setAttribute("loginname", user.getUsername());

            List<Permission> permissionList = permissionService.findPermissionByUserId(user.getId());
            req.setAttribute("permissionList",permissionList);
            //当前登录用户对应的可操作模块的所有url拼接成一个大的字符串
            StringBuffer sbf = new StringBuffer();
            for (Permission permission : permissionList) {
                sbf.append(permission.getUrl());
                sbf.append(',');
            }
            req.getSession().setAttribute("authorStr",sbf.toString());
            req.getRequestDispatcher("/WEB-INF/pages/home/main.jsp").forward(req, resp);
        } else {
            //登陆失败跳转至登录失败页面
            resp.sendRedirect(req.getContextPath() + "/failer.jsp");
        }
    }

    //跳转至home页面
    private void home(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/pages/home/home.jsp").forward(request,response);
    }

    //注销
    private void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie emailAndPasswordCookie = new Cookie("email&password", null);
        emailAndPasswordCookie.setMaxAge(0);
        emailAndPasswordCookie.setPath("/");
        resp.addCookie(emailAndPasswordCookie);

        Cookie autoLoginCookie = new Cookie("autoLogin", null);
        autoLoginCookie.setMaxAge(0);
        autoLoginCookie.setPath("/");
        resp.addCookie(autoLoginCookie);

        Cookie ckCookie = new Cookie("ck", null);
        ckCookie.setMaxAge(0);
        ckCookie.setPath("/");
        resp.addCookie(ckCookie);

        req.getSession().setAttribute("loginname",null);

        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }

    //修改密码
    private void updatePwd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String checkCode = req.getParameter("checkCode");

        //获取验证码
        String code = "123";
        code = loginService.getCheckCode();

        if (code == null) {
            req.setAttribute("error", "验证码超时,请重新获取");
        } else {
            if (!code.equalsIgnoreCase(checkCode)) {
                req.setAttribute("error", "验证码错误");
            } else {
                int i = loginService.updatePwd(email, password);
                //更改行数为0则为用户不存在
                if (i == 0) {
                    req.setAttribute("error", "用户不存在");
                } else {
                    this.logout(req, resp);
                    return;
                }
            }
        }
        req.getRequestDispatcher("/updatePwd.jsp").forward(req, resp);
    }

    //设置验证码
    private void setCheckCode(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置验证码并显示
        String code = loginService.setCheckCode();
        System.out.println("code = " + code);
        //跳转页面
        req.getRequestDispatcher("/updatePwd.jsp").forward(req, resp);
    }
}
