package com.itheima.web.controller.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import com.itheima.service.system.Impl.RoleServiceImpl;
import com.itheima.service.system.Impl.UserServiceImpl;
import com.itheima.service.system.RoleService;
import com.itheima.service.system.UserService;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/system/user")
public class UserServlet extends BaseServlet {

    private void toAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/pages/system/user/user-add.jsp").forward(request, response);
    }

    public void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = BeanUtil.fillBean(request, User.class);
        UserService userService = new UserServiceImpl();
        userService.save(user);
        response.sendRedirect(request.getContextPath()+"/system/user?operation=list");
    }

    public void deleteById(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ids = request.getParameter("id");
        String[] split = ids.split(",");
        for (int i = 0; i < split.length; i++) {
            userService.deleteById(split[i]);
        }
        response.sendRedirect(request.getContextPath()+"/system/user?operation=list");
    }

    private void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;

        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            System.out.println("转换前页面"+request.getParameter("page"));
            page = Integer.parseInt(request.getParameter("page"));
            System.out.println("转换后页面"+page);
        }
        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            System.out.println("转换前总页数"+request.getParameter("size"));
            size = Integer.parseInt(request.getParameter("size"));
            System.out.println("转换后总页数"+size);
        }

        if (StringUtils.isNotBlank(request.getParameter("searchName"))) {

            String name = request.getParameter("searchName");
            System.out.println(name);
            request.setAttribute("searchName", name);
            UserService userService = new UserServiceImpl();

            PageInfo pageInfo = userService.findByName(name,page, size);
            pageInfo.setSize(size);


            request.setAttribute("page", pageInfo);
            request.getRequestDispatcher("/WEB-INF/pages/system/user/user-list.jsp").forward(request, response);
            return;
        }

        UserService userService = new UserServiceImpl();
        PageInfo pageInfo = userService.findAll(page, size);
        request.setAttribute("page", pageInfo);
        pageInfo.setSize(size);
        request.getRequestDispatcher("/WEB-INF/pages/system/user/user-list.jsp").forward(request, response);
    }

/*    private void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;



        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            Integer.parseInt(request.getParameter("page"));
        }
        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            Integer.parseInt(request.getParameter("size"));
        }

        if (StringUtils.isNotBlank(request.getParameter("searchName"))) {

            String name = request.getParameter("searchName");

            request.setAttribute("searchName", name);
            UserService userService = new UserServiceImpl();

            PageInfo pageInfo = userService.findByName(name,page, size);
            pageInfo.setSize(size);
            request.setAttribute("page", pageInfo);
            request.getRequestDispatcher("/WEB-INF/pages/system/user/user-list.jsp").forward(request, response);
            return;
        }


        PageInfo pageInfo = userService.findAll(page, size);
        pageInfo.setSize(size);
        request.setAttribute("page", pageInfo);
        request.getRequestDispatcher("/WEB-INF/pages/system/user/user-list.jsp").forward(request, response);
    }*/

    private void findByName(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;
        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            Integer.parseInt(request.getParameter("page"));
            System.out.println(page);
        }
        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            Integer.parseInt(request.getParameter("size"));
            System.out.println(size+"dashdjkhqwasjk");
        }

        String name = request.getParameter("name");



        PageInfo pageInfo = userService.findByName(name,page, size);
        request.setAttribute("page", pageInfo);
        request.getRequestDispatcher("/WEB-INF/pages/system/user/user-list.jsp").forward(request, response);
    }

    private void updateStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String id = request.getParameter("id");
        User user = userService.findById(id);
        userService.update(user);
        response.sendRedirect(request.getContextPath()+"/system/user?operation=list");
    }

    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        System.out.println(id);

        User user = userService.findById(id);
        request.setAttribute("user", user);

        RoleService roleService = new RoleServiceImpl();
        List<Role> list = roleService.findByRoles(id);
        request.setAttribute("roles", list);

        request.getRequestDispatcher("/WEB-INF/pages/system/user/user-detail.jsp").forward(request, response);
    }

    private void updateRole(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String id = request.getParameter("id");
        User user = userService.findById(id);
        request.setAttribute("user", user);
        RoleService roleService = new RoleServiceImpl();
        List<Role> role = roleService.findByRoles(id);
        request.setAttribute("roles", role);

        List<Role> roleList = roleService.findAll();
        request.setAttribute("roleList", roleList);
        request.getRequestDispatcher("/WEB-INF/pages/system/user/user-role-add.jsp").forward(request, response);
    }

    private void addRoleToUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String userId = request.getParameter("userId");
        System.out.println(userId);
        String[] ids = request.getParameterValues("ids");
        System.out.println(ids);
        userService.addRoleToUser(userId, ids);

        response.sendRedirect(request.getContextPath()+"/system/user?operation=list");
    }
}
