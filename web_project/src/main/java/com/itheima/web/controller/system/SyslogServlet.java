package com.itheima.web.controller.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Syslog;
import com.itheima.service.system.Impl.UserServiceImpl;
import com.itheima.service.system.UserService;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@WebServlet("/system/syslog")
public class SyslogServlet extends BaseServlet {


    public void list(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        //进入列表页
        //获取数据
        int page = 1;
        int size = 10;
        if(StringUtils.isNotBlank(request.getParameter("page"))){
            page = Integer.parseInt(request.getParameter("page"));
        }
        if(StringUtils.isNotBlank(request.getParameter("size"))){
            size = Integer.parseInt(request.getParameter("size"));
        }

        if (StringUtils.isNotBlank(request.getParameter("searchName"))) {

            String name = request.getParameter("searchName");

            request.setAttribute("searchName", name);
            System.out.println(name);
            UserService userService = new UserServiceImpl();

            PageInfo pageInfo = syslogService.findByTime(name,page,size);
            pageInfo.setSize(size);

            request.setAttribute("page", pageInfo);
            request.getRequestDispatcher("/WEB-INF/pages/system/syslog/syslog-list.jsp").forward(request, response);
            return;
        }


        PageInfo all = syslogService.findAll(page, size);
        all.setSize(size);
        //将数据保存到指定的位置
        request.setAttribute("page",all);
        //跳转页面
        request.getRequestDispatcher("/WEB-INF/pages/system/syslog/syslog-list.jsp").forward(request,response);
    }

    public void deleteIds(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ids = request.getParameter("ids");
        //将数据获取到，封装成一个对象
        Syslog syslog = BeanUtil.fillBean(request,Syslog.class);
        //调用业务层接口save
//        DeptService deptService = new DeptServiceImpl();
        /*SyslogServiceImpl syslogService = new SyslogServiceImpl();*/
        syslogService.deleteIds(ids);
        //跳转回到页面list
        //list(request,response);
        response.sendRedirect(request.getContextPath()+"/system/syslog?operation=list");
    }

    public void downLoad(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //返回的数据类型为文件xlsx类型
        resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        String fileName = new String("日志文件.xlsx".getBytes(),"iso8859-1");
        resp.addHeader("Content-Disposition","attachment;fileName="+fileName);

        //生成报告的文件，然后传递到前端页面
        ByteArrayOutputStream os = syslogService.getReport();
        //获取产生响应的流对象
        ServletOutputStream sos = resp.getOutputStream();
        //将数据从原始的字节流对象中提取出来写入到servlet对应的输出流中
        os.writeTo(sos);
        //将输出流刷新
        sos.flush();
        os.close();

    }

    public void findByTime(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String values = request.getParameter("values");

        try {
            int page = 1;
            int size = 10;
            if (StringUtils.isNotBlank(request.getParameter("page"))) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            if (StringUtils.isNotBlank(request.getParameter("size"))) {
                size = Integer.parseInt(request.getParameter("size"));
            }
            PageInfo<Syslog> all = syslogService.findByTime(values,page, size);
            request.setAttribute("page", all);
            request.getRequestDispatcher("/WEB-INF/pages/system/syslog/syslog-list.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
