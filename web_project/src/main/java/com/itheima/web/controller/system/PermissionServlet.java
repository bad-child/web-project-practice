package com.itheima.web.controller.system;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.itheima.domain.system.Permission;
import com.itheima.service.system.PermissionService;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/system/permission")
public class PermissionServlet extends BaseServlet {

    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");

        int page=1;
        int size=5;

        if (StringUtils.isNotBlank(request.getParameter("page"))){
            page=Integer.parseInt(request.getParameter("page"));
        }

        if (StringUtils.isNotBlank(request.getParameter("size"))){
            size=Integer.parseInt(request.getParameter("size"));
        }
        if (StringUtils.isNotBlank(request.getParameter("searchName"))) {
            String searchName = request.getParameter("searchName");
            PageInfo<Permission> all = permissionService.findByName(searchName, page, size);
            all.setSize(size);
            request.setAttribute("page", all);
            request.setAttribute("searchName", searchName);

        } else {
            PageInfo<Permission> all = permissionService.findAll(page, size);
            all.setSize(size);
            request.setAttribute("page", all);
        }
        request.getRequestDispatcher("/WEB-INF/pages/system/permission/permission-list.jsp").forward(request, response);
    }

    public void toAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Permission> all = permissionService.findAll();
        List<Permission> list = new ArrayList<>();
        for (Permission p : all) {
            if (p.getType()!=2){
                list.add(p);
            }
        }
        request.setAttribute("permissionList", list);

        request.getRequestDispatcher("/WEB-INF/pages/system/permission/permission-add.jsp").forward(request, response);
    }

    public void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Permission permission = BeanUtil.fillBean(request, Permission.class, "yyyy-MM-dd");
        permissionService.save(permission);
        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");
    }

    public void toEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");
        Permission permission = permissionService.findById(id);
        request.setAttribute("permission", permission);


        List<Permission> all = permissionService.findAll();

        List<Permission> list = new ArrayList<>();
        for (Permission p : all) {
            if (p.getType() != 2){
                list.add(p);
            }
        }
        request.setAttribute("permissionList", list);
        request.getRequestDispatcher("/WEB-INF/pages/system/permission/permission-update.jsp").forward(request, response);
    }

    public void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Permission permission = BeanUtil.fillBean(request, Permission.class, "yyyy-MM-dd");
        permissionService.update(permission);
        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");
    }

    public void closeSelected(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // 获取删除用户id
        String ids = request.getParameter("ids");
        // 调用service删除
        permissionService.closeSelected(ids);

        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");
    }

    public void openSelected(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // 获取删除用户id
        String ids = request.getParameter("ids");
        // 调用service删除
        permissionService.openSelected(ids);

        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");
    }

}