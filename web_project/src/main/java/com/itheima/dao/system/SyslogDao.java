package com.itheima.dao.system;

import com.itheima.domain.system.Syslog;

import java.util.List;

public interface SyslogDao {
    
    void delete(String ids);

    void save(Syslog syslog);

    List<Syslog> findByTime(String time);

    List<Syslog> findAll();
}
