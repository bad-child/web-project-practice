package com.itheima.dao.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleDao {
    List<Role> findAll();

    List<Role> findRole(String id);

    void save(Role role);

    void delete(Role role);

    void update(Role role);

    Role findById(String id);

    void deleteRolePermission(String roleId);

    void saveRolePermission(@Param("roleId") String roleId, @Param("permissionId") String permissionId);

    List<Role> findRoles(String id);

    List<Role> findSearchName(String searchName);
}
