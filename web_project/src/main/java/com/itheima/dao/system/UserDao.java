package com.itheima.dao.system;

import com.itheima.domain.system.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {
    User findById(String id);

    User findByEmailAndPwd(@Param("email") String email, @Param("password") String password);

    int updatePwd(@Param("email") String email, @Param("password") String password);

    void save(User user);

    void delete(User user);

    void update(User user);

    List<User> findAll();

    void deleteById(String id);

    List<User> findByName(String name);

    User findRole(String id);

    void deleteByUserId(String id);

    void addRoleToUser(@Param("userId") String userId,@Param("roleId") String roleId);

}
