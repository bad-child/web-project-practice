package com.itheima.dao.system;

import com.itheima.domain.system.Permission;

import java.util.List;
import java.util.Map;

public interface PermissionDao {
    int save(Permission permission);

    void closeState(String id);

    int update(Permission permission);

    List<Permission> findAll();

    Permission findById(String id);

    List<Map> findAuthorDataByRoleId(String roleId);

    List<Permission> findPermissionByUserId(String id);

    void openState(String id);

    void deleteById(String id);

    List<Permission> findByName(String searchName);
}
