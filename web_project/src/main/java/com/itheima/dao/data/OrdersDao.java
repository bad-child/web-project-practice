package com.itheima.dao.data;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.data.Orders;

import java.util.List;

public interface OrdersDao {
    List<Orders> findAll();

    Orders findById(Orders orders);

    int save(Orders orders);

    int update(Orders orders);

    void changeOsByIds(String[] ids);

    void openOrderStatus(String[] ids);

    void closeOrderStatus(String[] ids);

    List<Orders> findByName(String searchName);
}
