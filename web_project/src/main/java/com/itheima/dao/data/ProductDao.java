package com.itheima.dao.data;

import com.itheima.domain.data.Product;

import java.util.List;

public interface ProductDao {
    List<Product> findByNum(String productNum);

    List<Product> findAll();

    Product findById(Product product);

    int save(Product product);

    int delete(Product product);

    int update(Product product);

    int delete(String id);

    Product findById(String id);

    void open(String[] split);

    void close(String[] split);
}
