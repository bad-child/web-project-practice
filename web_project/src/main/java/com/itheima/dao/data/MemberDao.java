package com.itheima.dao.data;

import com.itheima.domain.data.Member;

import java.util.List;

public interface MemberDao {
    List<Member> findAll();

    Member findById(Member member);

    int save(Member member);

    int delete(Member member);

    int update(Member member);
}
