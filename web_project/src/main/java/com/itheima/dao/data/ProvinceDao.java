package com.itheima.dao.data;

import com.itheima.domain.data.Province;

import java.util.List;

public interface ProvinceDao {
    List<Province> findAll();
}
