package com.itheima.dao.data;

import com.itheima.domain.data.Traveller;

import java.util.List;

public interface TravellerDao {
    List<Traveller> findAll();

    Traveller findById(Traveller traveller);

    int save(Traveller traveller);

    int delete(Traveller traveller);

    int update(Traveller traveller);
}
