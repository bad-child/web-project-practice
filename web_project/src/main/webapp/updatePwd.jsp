<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>数据 - AdminLTE2定制版 | Log in</title>

<meta
	content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
	name="viewport">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/plugins/ionicons/css/ionicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/plugins/iCheck/square/blue.css">
</head>

<body class="hold-transition login-page">

	<div class="login-box">
		<div class="login-logo">
			<a href="all-admin-index.html"><b>ITCAST</b>后台管理系统</a>
		</div>
		<!-- /.user-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">修改密码</p>

			<form action="${pageContext.request.contextPath}/user/login?operation=updatePwd" method="post">
				<div class="form-group has-feedback">
					<input id="email" type="text" name="email" class="form-control"
						placeholder="邮箱" value="${email}"><span
						class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input id="password" type="password" name="password" class="form-control"
						placeholder="新密码" value="${newPwd}"><span
						class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="text" name="checkCode" class="form-control"
						placeholder="验证码"><span
						class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<%--<div class="form-inline">
					<label for="vcode">验证码：</label>
					<input type="text" name="verifycode" class="form-control" id="verifycode" placeholder="请输入验证码" style="width: 120px;"/>
					<a href="javascript:refreshCode()"><img src="${pageContext.request.contextPath}/checkCode" title="看不清点击刷新" id="vcode" onclick="changeCode()"/></a>
				</div>--%>

				<div class="row">

					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label>${error}</label>
							<label id="rulesubmit"></label>
						</div>
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="button" onclick="changeCode()" class="btn btn-primary btn-block btn-flat">获取验证码</button>
						<button type="submit" class="btn btn-primary btn-block btn-flat">修改密码</button>
						<label hidden="true"><img id="vcode" src=""/></label>
					</div>
					<!-- /.col -->
				</div>
			</form>

			<a href="${pageContext.request.contextPath}/login.jsp">返回</a><br>

		</div>
		<!-- /.user-box-body -->
	</div>
	<!-- /.user-box -->

	<!-- jQuery 2.2.3 -->
	<!-- Bootstrap 3.3.6 -->
	<!-- iCheck -->
	<script
		src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/plugins/iCheck/icheck.min.js"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_square-blue',
				radioClass : 'iradio_square-blue',
				increaseArea : '20%' // optional
			});
		});

		function changeCode() {
			vcode.src="${pageContext.request.contextPath}/user/login?operation=setCheckCode";
			for(i = 1; i<= secs; i++) {
				window.setTimeout("update(" + i + ")",i * 1000);
			}
		}

	</script>

	<script>
		var secs = 10;
		var wait = secs * 1000;
		document.getElementById("rulesubmit").value = "验证码在(" + secs + ")秒内有效";
		//document.getElementById("rulesubmit").disabled = true;
		function update(num) {
			if(num == 0) {
				//document.getElementById("rulesubmit").readonly = readonly;
				document.getElementById("rulesubmit").value = "请重新获取验证码";}
			else {
				var printnr = (wait / 1000) - num;
				document.getElementById("rulesubmit").innerText = "验证码在(" + printnr + ")秒内有效";}
		}
		function timer() {
			document.getElementById("rulesubmit").disabled = true;
			//document.getElementById("rulesubmit").readonly = readonly;
			document.getElementById("rulesubmit").value = "请重新获取验证码";
		}
	</script>

	<script>
		//1.为表单登录绑定提交事件
		document.getElementById("form").onsubmit = function() {
			//2.获取填写的用户名和密码
			var username = document.getElementById("email").value;
			var password = document.getElementById("password").value;

			//3.判断用户名是否符合规则
			var reg1 = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
			if(!reg1.test(username)) {
				alert("用户名不符合规则，请输入正确的邮箱！");
				return false;
			}

			//4.判断密码是否符合规则
			/*var reg2 = /^[\d]{6}$/;
            if(!reg2.test(password)) {
                alert("密码不符合规则，请输入6位纯数字密码！");
                return false;
            }*/
			//5.如果所有条件都不满足，则提交表单
			return true;
		}
	</script>
</body>

</html>