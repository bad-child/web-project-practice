<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- 页面meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>数据 - AdminLTE2定制版</title>
<meta name="description" content="AdminLTE2定制版">
<meta name="keywords" content="AdminLTE2定制版">

<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
	name="viewport">

<link rel="stylesheet"
	href="../plugins/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css"/>
<link rel="stylesheet"
	href="../plugins/iCheck/square/blue.css"/>
<link rel="stylesheet"
	href="../plugins/morris/morris.css"/>
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css"/>
<link rel="stylesheet"
	href="../plugins/datepicker/datepicker3.css"/>
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker.css"/>
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"/>
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css"/>
<link rel="stylesheet"
	href="../plugins/treeTable/jquery.treetable.css"/>
<link rel="stylesheet"
	href="../plugins/treeTable/jquery.treetable.theme.default.css"/>
<link rel="stylesheet"
	href="../plugins/select2/select2.css">
<link rel="stylesheet"
	href="../plugins/colorpicker/bootstrap-colorpicker.min.css">
<link rel="stylesheet"
	href="../plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet"
	href="../plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet"
	href="../plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="../css/style.css">
<link rel="stylesheet"
	href="../plugins/ionslider/ion.rangeSlider.css">
<link rel="stylesheet"
	href="../plugins/ionslider/ion.rangeSlider.skinNice.css">
<link rel="stylesheet"
	href="../plugins/bootstrap-slider/slider.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">


			<!-- 内容头部 -->
			<section class="content-header">
			<h1>
				用户管理 <small>全部用户</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="../index.jsp"><i
						class="fa fa-dashboard"></i> 首页</a></li>
				<li><a
					href="../user/findAll.do">用户管理</a></li>

				<li class="active">全部用户</li>
			</ol>
			</section>
			<!-- 内容头部 /-->

				<!-- 正文区域 -->
				<section class="content"> <!-- .box-body -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">列表</h3>
					</div>

					<div class="box-body">

						<!-- 数据表格 -->
						<div class="table-box">

							<!--工具栏-->
							<div class="pull-left">
								<div class="form-group form-inline">
									<div class="btn-group">
										<button type="button" class="btn btn-default" title="新建" onclick="location.href='${pageContext.request.contextPath}/system/user?operation=toAdd'">
											<i class="fa fa-file-o"></i> 新建
										</button>
										<%--<button type="button" class="btn btn-default" title="删除" onclick="deleteByIds();">
											<i class="fa fa-trash"></i> 删除
										</button>--%>
										<button type="button" class="btn btn-default" title="刷新" onclick="location.href='${pageContext.request.contextPath}/system/user?operation=list'">
											<i class="fa fa-refresh"></i> 刷新
										</button>
									</div>
								</div>
							</div>
							<div class="box-tools pull-right">
								<div class="has-feedback">
									<%--<form action="${pageContext.request.contextPath}/system/user?operation=findByName" method="post">--%>
									<input type="text" class="form-control input-sm" name="name" id="search"
										placeholder="搜索" value="${searchName}" onchange="goPage(${page.pageNum})">
									<span class="glyphicon glyphicon-search form-control-feedback"></span>
									<%--</form>--%>
								</div>
							</div>
							<!--工具栏/-->

							<!--数据列表-->
							<table id="dataList" class="table table-bordered table-striped table-hover dataTable">
								<thead>
									<tr>
										<th class="" style="padding-right: 0px"><input
											id="selall" type="checkbox" class="icheckbox_square-blue">
										</th>
										<th class="sorting_asc">ID</th>
										<th class="sorting_desc">用户名</th>
										<th class="sorting_asc sorting_asc_disabled">邮箱</th>
										<th class="sorting_desc sorting_desc_disabled">联系电话</th>
										<th class="sorting">状态</th>
										<th class="text-center">操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${page.list}" varStatus="s" var="user">
									<tr>
										<td><input name="ids" id="ids" type="checkbox" value="${user.id}"></td>
										<td>${user.id}</td>
										<td>${user.username}</td>
										<td>${user.email}</td>
										<td>${user.phoneNum}</td>
										<td>
										<form action="/system/user?operation=updateStatus&id=${user.id}" id="update">
											<c:if test="${user.status == 0}"><a href="/system/user?operation=updateStatus&id=${user.id}"}>点击开启</a></c:if>
											<c:if test="${user.status == 1}"><a href="/system/user?operation=updateStatus&id=${user.id}"}>点击关闭</a></c:if>
											</td>
										</form>
										<td class="text-center">
											<a href="${pageContext.request.contextPath}/system/user?operation=detail&id=${user.id}" class="btn bg-olive btn-xs">详情</a>
											<a href="${pageContext.request.contextPath}/system/user?operation=updateRole&id=${user.id}" class="btn bg-olive btn-xs">更改角色</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
								<!--
                            <tfoot>
                            <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th>Engine version</th>
                            <th>CSS grade</th>
                            </tr>
                            </tfoot>-->
							</table>
							<!--数据列表/-->

						</div>
						<!-- 数据表格 /-->

					</div>
					<!-- /.box-body -->

					<!-- .box-footer-->
					<div class="box-footer">

						<div class="pull-left">
							<div class="form-group form-inline">
								总共${page.pages} 页，共${page.total} 条数据。 每页
								<select id="select" class="form-control" onchange="goSize(this.value)">
									<option ${page.size==3?'selected':''}>3</option>
									<option ${page.size==5?'selected':''}>5</option>
									<option ${page.size==10?'selected':''}>10</option>
									<option ${page.size==15?'selected':''}>15</option>
								</select> 条
							</div>
						</div>

						<div class="box-tools pull-right">
							<ul class="pagination" style="margin: 0px;">
								<li>
									<a href="javascript:goPage(1)" aria-label="Previous">首页</a>
								</li>
								<li><a href="javascript:goPage(${page.prePage})">上一页</a></li>
								<c:forEach begin="${page.navigateFirstPage}" end="${page.navigateLastPage}" var="i">
									<li class="paginate_button ${page.pageNum==i ? 'active':''}"><a href="javascript:goPage(${i})">${i}</a></li>
								</c:forEach>
								<li><a href="javascript:goPage(${page.nextPage})">下一页</a></li>
								<li>
									<a href="javascript:goPage(${page.pages})" aria-label="Next">尾页</a>
								</li>
							</ul>
						</div>
						<form id="pageForm" action="${pageContext.request.contextPath}/system/user?operation=list" method="post">
							<input type="hidden" name="page" id="page">
							<input type="hidden" name="size" id="size">
							<input type="hidden" name="searchName" id="searchName" value="${searchName}">
						</form>

					</div>
				</div>
				</section>
				<!-- 正文区域 /-->



		<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
		<script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="../plugins/raphael/raphael-min.js"></script>
		<script src="../plugins/morris/morris.min.js"></script>
		<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
		<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
		<script src="../plugins/knob/jquery.knob.js"></script>
		<script src="../plugins/daterangepicker/moment.min.js"></script>
		<script src="../plugins/daterangepicker/daterangepicker.js"></script>
		<script src="../plugins/daterangepicker/daterangepicker.zh-CN.js"></script>
		<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
		<script
			src="../plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
		<script
			src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="../plugins/fastclick/fastclick.js"></script>
		<script src="../plugins/iCheck/icheck.min.js"></script>
		<script src="../plugins/adminLTE/js/app.min.js"></script>
		<script src="../plugins/treeTable/jquery.treetable.js"></script>
		<script src="../plugins/select2/select2.full.min.js"></script>
		<script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
		<script
			src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.zh-CN.js"></script>
		<script src="../plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
		<script
			src="../plugins/bootstrap-markdown/locale/bootstrap-markdown.zh.js"></script>
		<script src="../plugins/bootstrap-markdown/js/markdown.js"></script>
		<script src="../plugins/bootstrap-markdown/js/to-markdown.js"></script>
		<script src="../plugins/ckeditor/ckeditor.js"></script>
		<script src="../plugins/input-mask/jquery.inputmask.js"></script>
		<script
			src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script src="../plugins/chartjs/Chart.min.js"></script>
		<script src="../plugins/flot/jquery.flot.min.js"></script>
		<script src="../plugins/flot/jquery.flot.resize.min.js"></script>
		<script src="../plugins/flot/jquery.flot.pie.min.js"></script>
		<script src="../plugins/flot/jquery.flot.categories.min.js"></script>
		<script src="../plugins/ionslider/ion.rangeSlider.min.js"></script>
		<script src="../plugins/bootstrap-slider/bootstrap-slider.js"></script>
		<script>
			//实现获取复选框ID，实现删除功能
			function deleteByIds(){
				//获取复选对象
				var ids=document.getElementsByName("ids");
				var arr=new Array();
				//alert("ok:"+ids.length);
				for(var i=0;i<ids.length;i++){
					//判断出被选中的复选框
					if(ids[i].checked){//true
						//alert(ids[i].user.id);
						//arr[i]=ids[i].value;
						arr.push(ids[i].value)
					}
				}
				//alert("ok:"+arr);
				//访问controller
				location.href="${pageContext.request.contextPath}/system/user?operation=deleteById&id="+arr;
			}
			
			/*function findByName() {
				document.getElementById("search").submit();
			}*/


/*
			<form id="pageForm" action="${param.pageUrl}" method="post">
					<input type="hidden" name="page" id="pageNum" value="${page.pageNum}">
					<%--<input type="text" value="${page.pageSize}">--%>
					<select name="size" id="pageSize" onchange="goPage(${page.pageNum})">
					<option class="p" value="3" ${page.pageSize==3?'selected':''}>3</option>
					<option class="p" value="5"  ${page.pageSize==5?'selected':''}>5</option>
					<option class="p" value="10"  ${page.pageSize==10?'selected':''}>10</option>

					</select>
					</form>
					<script>
					function goPage(page) {
						var elementsByClassName = document.getElementsByClassName("p");
						let size;
						for (let i = 0; i < elementsByClassName.length; i++) {
							if (elementsByClassName[i].selected) {
								size=elementsByClassName[i].value;
							}
						}

						document.getElementById("pageNum").value = page;
						document.getElementById("pageSize").value = size;
						document.getElementById("pageForm").submit()
					}*/

			function goPage(page) {
				document.getElementById("page").value = page;

				document.getElementById("size").value = document.getElementById("select").value;

				document.getElementById("searchName").value = document.getElementById("search").value;

				document.getElementById("pageForm").submit();
			}

			function goSize(size) {
				document.getElementById("size").value = size;

				document.getElementById("searchName").value = document.getElementById("searchName").value;

				document.getElementById("pageForm").submit();
			}


			/*function updateStatue() {
				if (confirm("是否确认修改状态？")) {
					document.getElementById("update").submit;
				}
			}*/



			$(document).ready(function() {
				// 选择框
				$(".select2").select2();

				// WYSIHTML5编辑器
				$(".textarea").wysihtml5({
					locale : 'zh-CN'
				});
			});

			// 设置激活菜单
			function setSidebarActive(tagUri) {
				var liObj = $("#" + tagUri);
				if (liObj.length > 0) {
					liObj.parent().parent().addClass("active");
					liObj.addClass("active");
				}
			}

			$(document)
					.ready(
							function() {

								// 激活导航位置
								setSidebarActive("admin-datalist");

								// 列表按钮 
								$("#dataList td input[type='checkbox']")
										.iCheck(
												{
													checkboxClass : 'icheckbox_square-blue',
													increaseArea : '20%'
												});
								// 全选操作 
								$("#selall")
										.click(
												function() {
													var clicks = $(this).is(
															':checked');
													if (!clicks) {
														$(
																"#dataList td input[type='checkbox']")
																.iCheck(
																		"uncheck");
													} else {
														$(
																"#dataList td input[type='checkbox']")
																.iCheck("check");
													}
													$(this).data("clicks",
															!clicks);
												});
							});
		</script>
</body>

</html>