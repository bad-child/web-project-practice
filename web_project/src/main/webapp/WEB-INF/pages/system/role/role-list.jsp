<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- 页面meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>数据 - AdminLTE2定制版</title>
<meta name="description" content="AdminLTE2定制版">
<meta name="keywords" content="AdminLTE2定制版">

<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
	name="viewport">

<link rel="stylesheet"
	href="../plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<link rel="stylesheet"
	href="../plugins/iCheck/square/blue.css">
<link rel="stylesheet"
	href="../plugins/morris/morris.css">
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet"
	href="../plugins/datepicker/datepicker3.css">
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="../plugins/treeTable/jquery.treetable.css">
<link rel="stylesheet"
	href="../plugins/treeTable/jquery.treetable.theme.default.css">
<link rel="stylesheet"
	href="../plugins/select2/select2.css">
<link rel="stylesheet"
	href="../plugins/colorpicker/bootstrap-colorpicker.min.css">
<link rel="stylesheet"
	href="../plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet"
	href="../plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet"
	href="../plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="../css/style.css">
<link rel="stylesheet"
	href="../plugins/ionslider/ion.rangeSlider.css">
<link rel="stylesheet"
	href="../plugins/ionslider/ion.rangeSlider.skinNice.css">
<link rel="stylesheet"
	href="../plugins/bootstrap-slider/slider.css">
</head>



<body class="hold-transition skin-blue sidebar-mini">
<%--

	<div class="wrapper">

		<!-- 页面头部 -->
		<jsp:include page="/pages/home/header.jsp"></jsp:include>
		<!-- 页面头部 /-->

		<!-- 导航侧栏 -->
		<jsp:include page="/pages/home/aside.jsp"></jsp:include>
		<!-- 导航侧栏 /-->

		<!-- 内容区域 -->
		<div class="content-wrapper">
--%>

			<!-- 内容头部 -->
			<section class="content-header">
			<h1>
				角色管理 <small>全部角色</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="../index.jsp"><i
						class="fa fa-dashboard"></i> 首页</a></li>
				<li><a
					href="../role/findAll.do">角色管理</a></li>

				<li class="active">全部角色</li>
			</ol>
			</section>
			<!-- 内容头部 /-->

				<!-- 正文区域 -->
				<section class="content">
				<!-- .box-body -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">列表</h3>
					</div>

					<div class="box-body">

						<!-- 数据表格 -->
						<div class="table-box">

							<!--工具栏-->

							<div class="pull-left">
								<div class="form-group form-inline">
									<div class="btn-group">
										<button type="button" class="btn btn-default" title="新建" onclick="location.href='${pageContext.request.contextPath}/system/role?operation=toAdd'">
											<i class="fa fa-file-o"></i> 新建
										</button>

										<button type="button" class="btn btn-default" title="关闭" onclick='deleteByIds();'>
											<i class="fa fa-trash"></i> 关闭
										</button>


										<button type="button" class="btn btn-default" title="刷新" onclick="location.href='${pageContext.request.contextPath}/system/role?operation=list'">
											<i class="fa fa-refresh"></i> 刷新
										</button>
									</div>
								</div>
							</div>


							<div class="box-tools pull-right">
								<div class="has-feedback">
									<input type="text" id="nameId" class="form-control input-sm" onblur="findByName()"
										   value="${searchName}">
									<span class="glyphicon glyphicon-search form-control-feedback"></span>
								</div>
							</div>



							<template>
								<el-select v-model="value" filterable placeholder="请选择">
									<el-option
											v-for="item in options"
											:key="item.value"
											:label="item.label"
											:value="item.value">
									</el-option>
								</el-select>
							</template>
							<!--工具栏/-->

							<!--数据列表-->
							<form action="${ctx}/system/role?operation=delete" id="form" method="post">
							<table id="dataList"
								class="table table-bordered table-striped table-hover dataTable">
								<thead>
									<tr>
										<th class="" style="padding-right: 0px"><input
											id="selall" type="checkbox" class="icheckbox_square-blue">
										</th>
										<th class="sorting_asc">ID</th>
										<th class="sorting_desc">角色名称</th>
										<th class="sorting_asc sorting_asc_disabled">描述</th>
										<th class="sorting_asc sorting_asc_disabled">状态</th>
										<th class="text-center">操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${page.list}" var="role" varStatus="i">
									<tr>
										<td><input name="ids" type="checkbox" value="${role.id}"></td>
										<td>${i.count}</td>
										<td>${role.roleName}</td>
										<td>${role.roleDesc}</td>
										<td>
											<c:if test="${role.status == '0'}"><a href="/system/role?operation=updateStatus&id=${role.id}"}>关闭</a></c:if>
											<c:if test="${role.status == '1'}"><a href="/system/role?operation=updateStatus&id=${role.id}"}>开启</a></c:if>
										</td>
										<td class="text-center">
											<a href="${ctx}/system/role?operation=findById&id=${role.id}" class="btn bg-olive btn-xs">角色详情</a>
											<a href="${pageContext.request.contextPath}/system/role?operation=toEdit&id=${role.id}" class="btn bg-olive btn-xs">角色编辑</a>
											<a href="${pageContext.request.contextPath}/system/role?operation=author&id=${role.id}" class="btn bg-olive btn-xs">更改权限资源</a>
										</td>
									</tr>
								</c:forEach>

								</tbody>
							</table>
							</form>
							<!--数据列表/-->
						</div>
						<!-- 数据表格 /-->
					</div>
					<!-- /.box-body -->

					<!-- .box-footer-->
					<div class="box-footer">

						<div class="pull-left">
							<div class="form-group form-inline">
								总共${page.pages} 页，共${page.total} 条数据。 每页
								<select id="select" class="form-control" onchange="goSize(this.value)">
									<option ${page.size==3?'selected':''}>3</option>
									<option ${page.size==5?'selected':''}>5</option>
									<option ${page.size==10?'selected':''}>10</option>
									<option ${page.size==15?'selected':''}>15</option>
								</select> 条
							</div>
						</div>

						<div class="box-tools pull-right">
							<ul class="pagination" style="margin: 0px;">
								<li>
									<a href="javascript:goPage(1)" aria-label="Previous">首页</a>
								</li>
								<li><a href="javascript:goPage(${page.prePage})">上一页</a></li>
								<c:forEach begin="${page.navigateFirstPage}" end="${page.navigateLastPage}" var="i">
									<li class="paginate_button ${page.pageNum==i ? 'active':''}"><a href="javascript:goPage(${i})">${i}</a></li>
								</c:forEach>
								<li><a href="javascript:goPage(${page.nextPage})">下一页</a></li>
								<li>
									<a href="javascript:goPage(${page.pages})" aria-label="Next">尾页</a>
								</li>
							</ul>
						</div>
						<form id="pageForm" action="${pageContext.request.contextPath}/system/role?operation=list" method="post">
							<input type="hidden" name="page" id="pageNum">
							<input type="hidden" name="size" id="size">
							<input type="hidden" name="searchName" id="searchName" value="${searchName}">
						</form>

					</div>



				</div>

				</section>
				<!-- 正文区域 /-->
			<%--<div class="box-footer">
				<jsp:include page="../../common/page.jsp">
					<jsp:param value="${ctx}/system/module?operation=list" name="pageUrl"/>
				</jsp:include>
			</div>--%>
<%--			</div>
			<!-- @@close -->
			<!-- 内容区域 /-->

			<!-- 底部导航 -->
			<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0.8
			</div>
			<strong>Copyright &copy; 2014-2017 <a
				href="http://www.itcast.cn">研究院研发部</a>.
			</strong> All rights reserved. </footer>
			<!-- 底部导航 /-->

		</div>--%>

		<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
		<script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="../plugins/raphael/raphael-min.js"></script>
		<script src="../plugins/morris/morris.min.js"></script>
		<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
		<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
		<script src="../plugins/knob/jquery.knob.js"></script>
		<script src="../plugins/daterangepicker/moment.min.js"></script>
		<script src="../plugins/daterangepicker/daterangepicker.js"></script>
		<script src="../plugins/daterangepicker/daterangepicker.zh-CN.js"></script>
		<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
		<script
			src="../plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
		<script
			src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="../plugins/fastclick/fastclick.js"></script>
		<script src="../plugins/iCheck/icheck.min.js"></script>
		<script src="../plugins/adminLTE/js/app.min.js"></script>
		<script src="../plugins/treeTable/jquery.treetable.js"></script>
		<script src="../plugins/select2/select2.full.min.js"></script>
		<script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
		<script
			src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.zh-CN.js"></script>
		<script src="../plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
		<script
			src="../plugins/bootstrap-markdown/locale/bootstrap-markdown.zh.js"></script>
		<script src="../plugins/bootstrap-markdown/js/markdown.js"></script>
		<script src="../plugins/bootstrap-markdown/js/to-markdown.js"></script>
		<script src="../plugins/ckeditor/ckeditor.js"></script>
		<script src="../plugins/input-mask/jquery.inputmask.js"></script>
		<script
			src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script src="../plugins/chartjs/Chart.min.js"></script>
		<script src="../plugins/flot/jquery.flot.min.js"></script>
		<script src="../plugins/flot/jquery.flot.resize.min.js"></script>
		<script src="../plugins/flot/jquery.flot.pie.min.js"></script>
		<script src="../plugins/flot/jquery.flot.categories.min.js"></script>
		<script src="../plugins/ionslider/ion.rangeSlider.min.js"></script>
		<script src="../plugins/bootstrap-slider/bootstrap-slider.js"></script>
		<script>

			function findName() {
				let searchName = document.getElementById("nameId").value;
				return searchName;
			}

			function findByName() {
				let searchName = document.getElementById("nameId").value;
				location.href="${pageContext.request.contextPath}/system/role?operation=list&searchName="+findName();
			}


			function goPage(page) {
				document.getElementById("pageNum").value = page;

				document.getElementById("size").value = document.getElementById("select").value;

				document.getElementById("searchName").value = findName();

				document.getElementById("pageForm").submit()
			}

			function goSize(size) {
				document.getElementById("size").value = size;

				document.getElementById("searchName").value = findName();

				document.getElementById("pageForm").submit();
			}


            //实现获取复选框ID，实现关闭功能
		   function deleteByIds() {
			   //1.获取选中复选框的个数
			   var len = $("input[name='ids']:checked").length;
			   if (len == 0) {
				   alert("请勾选您要关闭的角色!");
				   return;
			   }
			   if (confirm("您确认要关闭选中角色么?")) {
				   $("#form").submit();
			   }
		   }

			$(document).ready(function() {
				// 选择框
				$(".select2").select2();

				// WYSIHTML5编辑器
				$(".textarea").wysihtml5({
					locale : 'zh-CN'
				});
			});
			// 设置激活菜单
			function setSidebarActive(tagUri) {
				var liObj = $("#" + tagUri);
				if (liObj.length > 0) {
					liObj.parent().parent().addClass("active");
					liObj.addClass("active");
				}
			}
			$(document)
					.ready(
							function() {

								// 激活导航位置
								setSidebarActive("admin-datalist");

								// 列表按钮 
								$("#dataList td input[type='checkbox']")
										.iCheck(
												{
													checkboxClass : 'icheckbox_square-blue',
													increaseArea : '20%'
												});
								// 全选操作 
								$("#selall")
										.click(
												function() {
													var clicks = $(this).is(
															':checked');
													if (!clicks) {
														$(
																"#dataList td input[type='checkbox']")
																.iCheck(
																		"uncheck");
													} else {
														$(
																"#dataList td input[type='checkbox']")
																.iCheck("check");
													}
													com.itheima.domain.basicData("clicks",
															!clicks);
												});
							});
		</script>
</body>

</html>