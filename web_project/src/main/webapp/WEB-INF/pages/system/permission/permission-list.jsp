<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ include file="../../base.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">

    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/ionicons/css/ionicons.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/iCheck/square/blue.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/morris/morris.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/treeTable/jquery.treetable.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/treeTable/jquery.treetable.theme.default.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/select2/select2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/adminLTE/css/AdminLTE.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/adminLTE/css/skins/_all-skins.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/css/style.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/ionslider/ion.rangeSlider.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/ionslider/ion.rangeSlider.skinNice.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/bootstrap-slider/slider.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">


        <section class="content-header">
            <h1>
                资源权限管理
                <small>全部权限</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                        class="fa fa-dashboard"></i> 首页</a></li>
                <li><a
                        href="${pageContext.request.contextPath}/system/permission?operation=list">资源权限管理</a></li>

                <li class="active">全部权限</li>
            </ol>
        </section>
        <!-- 内容头部 /-->

        <!-- 正文区域 -->
        <section class="content"> <!-- .box-body -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">列表</h3>
                </div>

                <div class="box-body">

                    <!-- 数据表格 -->
                    <div class="table-box">

                        <!--工具栏-->
                        <div class="pull-left">
                            <div class="form-group form-inline">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" title="新建"
                                            onclick="location.href='${pageContext.request.contextPath}/system/permission?operation=toAdd'">
                                        <i class="fa fa-file-o"></i> 新建
                                    </button>
                                    <button type="button" class="btn btn-default" title="开启" onclick='openSelected()'>
                                        <i class="fa fa-trash"></i> 开启
                                    </button>
                                    <button type="button" class="btn btn-default" title="屏蔽" onclick='closeSelected()'>
                                        <i class="fa fa-trash"></i> 屏蔽
                                    </button>
                                    <button type="button" class="btn btn-default" title="刷新"
                                            onclick="window.location.reload();">
                                        <i class="fa fa-refresh"></i> 刷新
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input id="searchData" type="text" class="form-control input-sm" placeholder="搜索" onblur="findByName()"
                                       value="${searchName}">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!--工具栏/-->

                        <!--数据列表-->
                        <form id="form" action="${pageContext.request.contextPath}/system/permission?operation=closeSelected" method="post">
                            <table id="dataList" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                <tr>
                                    <th class="" style="padding-right: 0px">
                                        <input id="selall" type="checkbox" class="icheckbox_square-blue"/>
                                    </th>
                                    <th class="sorting_asc">ID</th>
                                    <th class="sorting_desc">权限名称</th>
                                    <th class="sorting">类型</th>
                                    <th class="sorting">上级模块</th>
                                    <th class="sorting_asc sorting_asc_disabled">URL</th>
                                    <th class="sorting">状态</th>
                                    <th class="text-center">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${page.list}" var="o" varStatus="st">
                                    <tr>
                                        <td><input name="ids" type="checkbox" value="${o.permissionId}"></td>
                                        <td>${st.count}</td>
                                        <td><a href="#">${o.permissionName}</a></td>
                                        <td>${o.type==0?'主菜单':o.type==1?'二级菜单':'按钮'}</td>
                                        <td>${o.permission.permissionName}</td>
                                        <td>${o.url}</td>
                                        <td>${o.state==0?'停用':'启用'}</td>
                                        <td class="text-center">
                                            <a href="${pageContext.request.contextPath}/system/permission?operation=toEdit&id=${o.permissionId}" class="btn bg-olive btn-xs">编辑权限</a>
                                            <%--<a href="javascript:deleteOne(${o.permissionId})" class="btn bg-olive btn-xs">删除权限</a>--%>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <!--数据列表/-->
                        </form>
                    </div>
                    <!-- 数据表格 /-->

                </div>
                <!-- /.box-body -->


                <div class="box-footer">

                    <div class="pull-left">
                        <div class="form-group form-inline">
                            总共${page.pages} 页，共${page.total} 条数据。 每页
                            <select id="select" class="form-control" onchange="goSize(this.value)">
                                <option ${page.size==3?'selected':''}>3</option>
                                <option ${page.size==5?'selected':''}>5</option>
                                <option ${page.size==10?'selected':''}>10</option>
                                <option ${page.size==15?'selected':''}>15</option>
                            </select> 条
                        </div>
                    </div>

                    <div class="box-tools pull-right">
                        <ul class="pagination" style="margin: 0px;">
                            <li>
                                <a href="javascript:goPage(1)" aria-label="Previous">首页</a>
                            </li>
                            <li><a href="javascript:goPage(${page.prePage})">上一页</a></li>
                            <c:forEach begin="${page.navigateFirstPage}" end="${page.navigateLastPage}" var="i">
                                <li class="paginate_button ${page.pageNum==i ? 'active':''}"><a href="javascript:goPage(${i})">${i}</a></li>
                            </c:forEach>
                            <li><a href="javascript:goPage(${page.nextPage})">下一页</a></li>
                            <li>
                                <a href="javascript:goPage(${page.pages})" aria-label="Next">尾页</a>
                            </li>
                        </ul>
                    </div>
                    <form id="pageForm" action="${pageContext.request.contextPath}/system/permission?operation=list" method="post">
                        <input type="hidden" name="page" id="pageNum">
                        <input type="hidden" name="size" id="size">
                        <input type="hidden" name="searchName" id="searchName" value="${searchName}">
                    </form>
                </div>


                <!-- /.box-footer-->

            </div>

        </section>
        <!-- 正文区域 /-->


<script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/raphael/raphael-min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/morris/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="${pageContext.request.contextPath}/plugins/knob/jquery.knob.js"></script>
<script src="${pageContext.request.contextPath}/plugins/daterangepicker/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/daterangepicker/daterangepicker.js"></script>
<script src="${pageContext.request.contextPath}/plugins/daterangepicker/daterangepicker.zh-CN.js"></script>
<script src="${pageContext.request.contextPath}/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="${pageContext.request.contextPath}/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/fastclick/fastclick.js"></script>
<script src="${pageContext.request.contextPath}/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/adminLTE/js/app.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/treeTable/jquery.treetable.js"></script>
<script src="${pageContext.request.contextPath}/plugins/select2/select2.full.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.zh-CN.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-markdown/locale/bootstrap-markdown.zh.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-markdown/js/markdown.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-markdown/js/to-markdown.js"></script>
<script src="${pageContext.request.contextPath}/plugins/ckeditor/ckeditor.js"></script>
<script src="${pageContext.request.contextPath}/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="${pageContext.request.contextPath}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/chartjs/Chart.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/flot/jquery.flot.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/flot/jquery.flot.categories.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/ionslider/ion.rangeSlider.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document)
        .ready(
            function () {
                // 激活导航位置
                setSidebarActive("admin-datalist");
                // 列表按钮
                $("#dataList td input[type='checkbox']")
                    .iCheck(
                        {
                            checkboxClass: 'icheckbox_square-blue',
                            increaseArea: '20%'
                        });
                // 全选操作
                $("#selall")
                    .click(
                        function () {
                            var clicks = $(this).is(
                                ':checked');
                            if (!clicks) {
                                $(
                                    "#dataList td input[type='checkbox']")
                                    .iCheck(
                                        "uncheck");
                            } else {
                                $(
                                    "#dataList td input[type='checkbox']")
                                    .iCheck("check");
                            }
                            com.itheima.domain.basicData("clicks",
                                !clicks);
                        });
            });

    /*    function checkedAll(flag) {
            $("input[name='id']").prop("checked", flag);
        }*/
    function closeSelected(){
        //获取复选对象
        var flag=false;
        var ids=document.getElementsByName("ids");
        var arr=new Array();
        //alert("ok:"+ids.length);
        for(var i=0;i<ids.length;i++){
            //判断出被选中的复选框
            if(ids[i].checked){//true
                flag=true;
                //alert(ids[i].value);
                //arr[i]=ids[i].value;
                arr.push(ids[i].value)
            }
        }
        //alert("ok:"+arr);
        //访问controller

        if (!flag){
            alert("请选择要屏蔽的数据");
            return;
        }

        if (confirm("是否确认屏蔽")){
            location.href="${pageContext.request.contextPath}/system/permission?operation=closeSelected&ids="+arr;
        }
    }
    function openSelected(){
        //获取复选对象
        var flag=false;
        var ids=document.getElementsByName("ids");
        var arr=new Array();
        //alert("ok:"+ids.length);
        for(var i=0;i<ids.length;i++){
            //判断出被选中的复选框
            if(ids[i].checked){//true
                flag=true;
                //alert(ids[i].value);
                //arr[i]=ids[i].value;
                arr.push(ids[i].value)
            }
        }
        //alert("ok:"+arr);
        //访问controller

        if (!flag){
            alert("请选择要开启的数据");
            return;
        }

        if (confirm("是否确认开启")){
            location.href="${pageContext.request.contextPath}/system/permission?operation=openSelected&ids="+arr;
        }
    }
    function findName() {
        var searchName = document.getElementById("searchData").value;
        return searchName;
    }

    function findByName() {
        var searchName = document.getElementById("searchData").value;
        location.href="${pageContext.request.contextPath}/system/permission?operation=list&searchName="+findName();
    }
    function goPage(page) {
        document.getElementById("pageNum").value = page;

        document.getElementById("size").value = document.getElementById("select").value;

        document.getElementById("searchName").value = findName();

        document.getElementById("pageForm").submit()
    }

    function goSize(size) {
        document.getElementById("size").value = size;

        document.getElementById("searchName").value = findName();

        document.getElementById("pageForm").submit();
    }
</script>

</body>

</html>